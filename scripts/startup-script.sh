#!/usr/bin/env bash

export INSTANCE_TYPE="@INSTANCE_TYPE@" 
export CLUSTER_NAME="@CLUSTER_NAME@"

python /apps/slurm/scripts/cluster-services.py --config=update
python /apps/slurm/scripts/cluster-services.py --service=update-mounts
python /apps/slurm/scripts/cluster-services.py --service=setup-munge
python /apps/slurm/scripts/cluster-services.py --service=setup-slurmdb
python /apps/slurm/scripts/cluster-services.py --service=setup-slurm
python /apps/slurm/scripts/cluster-services.py --service=update-slurm-users


# Make root level directory for temporary test directories
mkdir /code-sandbox

# Copy fluid-runner scripts onto system
#git clone https://joe_fluidnumerics@bitbucket.org/fluidnumerics/fluid-runner.git /apps/fluid-runner 
