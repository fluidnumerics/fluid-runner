#!/usr/bin/python


import os
import subprocess
import shlex

class fluid_runner:

    def __init__(self, config={}, status='new'):
        self.config = config 
        self.status = status 


    def setup_tmp_dir( runner ):
        """Creates a temporary directory for building and testing HPC application."""
        # We create a temporary directory so that each build and run-test can be created without overlap and all artifacts can be tar'ed and saved
        # The repository at the particular commit id is cloned to this temporary directory
        # A new config dictionary with the temporary directory appended is returned

        rand_postfix = ''.join(random.choice(chars) for _ in range(size))
        subprocess.call( shlex.split('git clone {GIT_URL} --depth 1 --branch {GIT_BRANCH} /code-sandbox/{GIT_REPO_NAME}_{GIT_BRANCH}_{GIT_SHA}_{RAND}'.format( 
                                      GIT_URL = runner.config['git_url'],
                                      GIT_BRANCH = runner.config['git_branch'],
                                      GIT_REPO_NAME = runner.config['git_repo_name'],
                                      GIT_SHA = runner.config['git_sha'],
                                      RAND = rand_postfix )) )
        new_config = runner.config
        new_config['tmp_dir'] = '/code-sandbox/{GIT_REPO_NAME}_{GIT_BRANCH}_{GIT_SHA}_{RAND}'.format( 
                                      GIT_REPO_NAME = runner.config['git_repo_name'],
                                      GIT_BRANCH = runner.config['git_branch'],
                                      GIT_SHA = runner.config['git_sha'],
                                      RAND = rand_postfix )) )
    
        return new_config
                                     
    #[END setup_tmp_dir]
    
    def do_custom_build( runner ):
        """Execute custom build provided by the user"""
    
        batch_file=""""#!/bin/bash
#SBATCH --job-name={JOB_NAME}
#SBATCH --output={TMP_DIR}/build.output
#SBATCH --ntasks=1
#SBATCH --time={BUILD_TIMEOUT}
#SBATCH --partition={PARTITION}

cd {TMP_DIR}
{BUILD_INSTRUCTIONS}

""".format( JOB_NAME = runner.config['tmp_dir'].split('/')[-1],
            TMP_DIR = runner.config['tmp_dir'],
            BUILD_TIMEOUT = runner.config['build_timeout'],
            PARTITION = runner.config['build_platform'],
            BUILD_INSTRUCTIONS = runner.config['build_instructions'] )


        f = open( runner.config['tmp_dir']+'/build.batch', 'w' )
        f.write( batch_file )
        f.close()

        slurm_jobid = os.popen( '/apps/slurm/current/bin/sbatch {TMP_DIR}/build.batch'.format( TMP_DIR=runner.config['tmp_dir'] ) ).read().split()[-1]

        # Wait for job to finish
        while True:
        
            job_stat = os.popen('/apps/slurm/current/bin/scontrol show job={0} | grep JobState'.format(job_id)).read()
            # If slurm squeue reports an error, the job is no longer in the queue
            # and we assume the job is complete
            if not 'JobState' in job_stat or 'COMPLETE' in job_stat:
              proc = subprocess.Popen( shlex.split('/apps/slurm/current/bin/sacct --jobs={JOB_ID} --format=cputime,exitcode,start,end -P'), stdout=subprocess.PIPE, stderr=subprocess.STDOUT )
              output = proc.communicate( )

              cputime = output.split('|')[0]
              exitcode = output.split('|')[1]
              starttime = output.split('|')[2]
              endtime = output.split('|')[3]

              if os.path.isfile( runner.config['tmp_dir']+ "/build.output" ):
                f = open(  runner.config['tmp_dir']+ "/build.output" )
                build_log = f.read()
                f.close()


        response = { 'cpu_time':cputime, 'exitcode':exitcode, 'start_time':starttime, 'end_time':endtime, 'build_log':build_log }
        return response

         
    
    #[END do_custom_build]

    def do_singularity_build( runner ):
        """Execute a singularity build in the temporary directory"""
    
        batch_file=""""#!/bin/bash
#SBATCH --job-name={JOB_NAME}
#SBATCH --output={TMP_DIR}/build.output
#SBATCH --ntasks=1
#SBATCH --time={BUILD_TIMEOUT}
#SBATCH --partition={PARTITION}

module load singularity
cd {TMP_DIR}
singularity build --notest {SINGULARITY_IMAGE_FILE} {TMP_DIR}/{SINGULARITY_DEFINITION_FILE}
touch build.check

""".format( JOB_NAME = runner.config['tmp_dir'].split('/')[-1],
            TMP_DIR = runner.config['tmp_dir'],
            BUILD_TIMEOUT = runner.config['build_timeout'],
            PARTITION = runner.config['build_platform'],
            SINGULARITY_IMAGE_FILE = runner.config['singularity_image_file'],
            SINGULARITY_DEFINITION_FILE = runner.config['singularity_def_file'] )


        f = open( runner.config['tmp_dir']+'/build.batch', 'w' )
        f.write( batch_file )
        f.close()

        slurm_jobid = os.popen( '/apps/slurm/current/bin/sbatch {TMP_DIR}/build.batch'.format( TMP_DIR=runner.config['tmp_dir'] ) ).read().split()[-1]


        # Wait for job to finish
        while True:
        
            job_stat = os.popen('/apps/slurm/current/bin/scontrol show job={0} | grep JobState'.format(job_id)).read()
            # If slurm squeue reports an error, the job is no longer in the queue
            # and we assume the job is complete
            if not 'JobState' in job_stat or 'COMPLETE' in job_stat:
              proc = subprocess.Popen( shlex.split('/apps/slurm/current/bin/sacct --jobs={JOB_ID} --format=cputime,exitcode,start,end -P'), stdout=subprocess.PIPE, stderr=subprocess.STDOUT )
              output = proc.communicate( )

              cputime = output.split('|')[0]
              exitcode = output.split('|')[1]
              starttime = output.split('|')[2]
              endtime = output.split('|')[3]

              if os.path.isfile( runner.config['tmp_dir']+ "/build.output" ):
                f = open(  runner.config['tmp_dir']+ "/build.output" )
                build_log = f.read()
                f.close()


        response = { 'cpu_time':cputime, 'exitcode':exitcode, 'start_time':starttime, 'end_time':endtime, 'build_log':build_log }
        return response
    
    #[END do_singularity_build]

    def post_singularity_image( runner ) :
        """Post the singularity image to GCS Bucket."""


    #[END post_singularity_image]

#    def do_singularity_test( runner ):
#        """Execute a singularity test in the temporary directory"""
#    
#        os.chdir( runner_config['tmp_dir'] )
#        proc = subprocess.Popen( shlex.split('singularity test {SINGULARITY_IMAGE_FILE} {TMP_DIR}/{SINGULARITY_DEFINITION_FILE}'.format(
#                                     SINGULARITY_IMAGE_FILE = runner.config['singularity_image_file'],
#                                     TMP_DIR = runner.config['tmp_dir'],
#                                     SINGULARITY_DEFINITION_FILE = runner.config['singularity_def_file'])),
#                                     stdout = subprocess.PIPE,
#                                     stderr = subprocess.PIPE )
#
#        std_out, std_err = proc.communicate()
#        exit_code = proc.returncode()
#
#        response = { 'std_out':std_out, 'std_err':std_err, 'exit_code': exit_code }
#
#        return response
#         
#    
#    #[END do_singularity_build]
    
    def tar_up_artifacts( runner_config ):
        """Dumps the config to tmp_dir and make a tarball of the tmp_dir to upload to storage bucket"""
    
    #[END tar_up_artifacts]
